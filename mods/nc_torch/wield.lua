-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs, vector
    = minetest, nodecore, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local litgroup = {}
minetest.after(0, function()
		for k, v in pairs(minetest.registered_items) do
			if v.groups.torch_lit then
				litgroup[k] = true
			end
		end
	end)
local function islit(stack)
	return stack and litgroup[stack:get_name()]
end

local function snuffinv(player, inv, i)
	nodecore.sound_play("nc_fire_snuff", {object = player, gain = 0.5})
	inv:set_stack("main", i, "nc_fire:lump_ash")
end

local ambtimers = {}
minetest.register_globalstep(function()
		local now = nodecore.gametime
		for _, player in pairs(minetest.get_connected_players()) do
			local inv = player:get_inventory()
			local ppos = player:get_pos()

			-- Snuff all torches if doused in water.
			local hpos = vector.add(ppos, {x = 0, y = 1, z = 0})
			local head = minetest.get_node(hpos).name
			if minetest.get_item_group(head, "water") > 0 then
				for i = 1, inv:get_size("main") do
					local stack = inv:get_stack("main", i)
					if islit(stack) then snuffinv(player, inv, i) end
				end
			elseif islit(player:get_wielded_item()) then
				-- Wield ambiance
				local name = player:get_player_name()
				local t = ambtimers[name] or 0
				if t <= now then
					ambtimers[name] = now + 1
					nodecore.sound_play("nc_fire_flamy",
						{object = player, gain = 0.1})
				end
			end
		end
	end)
